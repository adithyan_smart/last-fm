package com.henote.adithyan.lastfm.presenters;

import android.content.Context;

import com.henote.adithyan.lastfm.models.SearchModelImpl;
import com.henote.adithyan.lastfm.mvp.SearchListMVP;
import com.henote.adithyan.lastfm.pojos.Artist_;

import java.lang.ref.WeakReference;
import java.util.List;

public class SearchPresenterImpl implements SearchListMVP.SearchPresenter, SearchListMVP.SearchModel.onSearchResponseListener {

    private Context mContext;
    private SearchListMVP.SearchModel mSearchModel;
    private SearchListMVP.SearchView mSearchView;

    public SearchPresenterImpl(Context context, SearchListMVP.SearchView searchView){
        mContext = new WeakReference<>(context).get();
        mSearchView = searchView;
        mSearchModel = new SearchModelImpl(mContext,this);
    }
    @Override
    public void onCallSearchApi(String query, int page, int searchId) {
        if (mSearchView != null) {
            mSearchView.showProgress();
            mSearchModel.onCallSearchApi(query, page, searchId);
        }
    }

    @Override
    public void onDestroy() {
        mSearchView = null;
    }

    @Override
    public void onSuccess(List<Artist_> artistList,int searchId) {
        if (mSearchView != null) {
            mSearchView.hideProgress();
            mSearchView.onRefreshData(artistList,searchId);
        }
    }

    @Override
    public void onFailure(String errorMessage,int searchId) {
        if (mSearchView != null) {
            mSearchView.hideProgress();
            mSearchView.onShowError(errorMessage, searchId);
        }
    }


}
