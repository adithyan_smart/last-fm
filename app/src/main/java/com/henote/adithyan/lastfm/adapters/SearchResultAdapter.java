package com.henote.adithyan.lastfm.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.henote.adithyan.lastfm.R;
import com.henote.adithyan.lastfm.pojos.Artist_;
import com.henote.adithyan.lastfm.pojos.Image;
import com.henote.adithyan.lastfm.utils.Utility;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * MOHAN on 17-09-2017.
 */

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder > {

    private Context mContext;
    private List<Artist_> mArtistList;

    public SearchResultAdapter(Context context, List<Artist_>  artistList){
        mContext = context;
        mArtistList = artistList;
    }
    @Override
    public SearchResultAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Artist_ artist = mArtistList.get(position);

        Picasso.with(mContext)
                .load(getMedium(artist.getImage()))
                .placeholder(R.drawable.thumb_placeholder)
                .into(holder.ivImage);

        holder.tvName.setText(artist.getName());
    }
    public String getMedium(List<Image> imageList){
        String imageUrl = "drawable://" + R.drawable.thumb_placeholder;
        for(Image image : imageList){
            if (image.getSize().equals("medium")){
                if (Utility.nullCheck(image.getText()))
                    imageUrl= image.getText();
            }
        }
        return imageUrl;
    }
    @Override
    public int getItemCount() {
        return mArtistList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.iv_image)
        ImageView ivImage;
        @BindView(R.id.tv_name)
        TextView tvName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
