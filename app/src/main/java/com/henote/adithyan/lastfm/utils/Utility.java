package com.henote.adithyan.lastfm.utils;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;


public class Utility {

    public static boolean nullCheck(String value) {
        return value != null && value.trim().length() != 0
                && !value.trim().contentEquals("null")
                && !value.trim().contentEquals("__NULL__")
                && !value.trim().contentEquals("false")
                && !value.trim().contentEquals("FALSE");
    }


    public static boolean isOnline(Context context) {
        if (context == null) return false;

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();


    }

    public static void hideKeyBoard(AppCompatActivity appCompatActivity){
        if (appCompatActivity != null){
            InputMethodManager imm = (InputMethodManager) appCompatActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(appCompatActivity.getCurrentFocus().getWindowToken(), 0);
        }
    }
}
