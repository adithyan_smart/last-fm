package com.henote.adithyan.lastfm.ui;

import android.content.Context;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.henote.adithyan.lastfm.R;
import com.henote.adithyan.lastfm.adapters.SearchResultAdapter;
import com.henote.adithyan.lastfm.mvp.SearchListMVP;
import com.henote.adithyan.lastfm.pojos.Artist_;
import com.henote.adithyan.lastfm.presenters.SearchPresenterImpl;
import com.henote.adithyan.lastfm.utils.RecyclerViewPositionHelper;
import com.henote.adithyan.lastfm.utils.Utility;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SearchListMVP.SearchView{

    private int mPageCount =1;
    private final int itemsPerPage=30;
    private boolean mIsApiCalled =false;
    private boolean isPaginated = false;
    private int mSearchId =0;
    private  int mFirstVisibleItem, mVisibleItemCount;
    private RecyclerViewPositionHelper mRecyclerViewHelper;
    private Context mContext;
    private SearchResultAdapter mSearchResultAdapter;
    private List<Artist_> mArtistList = new ArrayList<>();
    private SearchListMVP.SearchPresenter mSearchPresenter;
    @BindView(R.id.pb)
    ProgressBar pb;
    @BindView(R.id.rv_result)
    RecyclerView rvResult;
    @BindView(R.id.tv_no_result)
    TextView tvNoResult;
    @BindView(R.id.et_search)
    EditText etSearch;
    @BindView(R.id.srl_result)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
        initListener();
    }

    private void init() {
        mContext = MainActivity.this;
        mSearchPresenter = new SearchPresenterImpl(mContext,this);
        mSearchResultAdapter = new SearchResultAdapter(mContext,mArtistList);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvResult.setLayoutManager(mLinearLayoutManager);
        rvResult.setAdapter(mSearchResultAdapter);
    }

    private void initListener() {

        mRecyclerViewHelper = RecyclerViewPositionHelper.createHelper(rvResult);
        rvResult.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {
                super.onScrollStateChanged(recyclerView, scrollState);
                Utility.hideKeyBoard(MainActivity.this);
                mVisibleItemCount = recyclerView.getChildCount();
                mFirstVisibleItem = mRecyclerViewHelper.findFirstVisibleItemPosition();
                int lastVisibleItem = mRecyclerViewHelper.findLastCompletelyVisibleItemPosition();
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();

                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                        && (mFirstVisibleItem + mVisibleItemCount) >= (mSearchResultAdapter.getItemCount() - mVisibleItemCount)) {
                    if (!mIsApiCalled && (0 == (mArtistList.size() % itemsPerPage))) {
                        mIsApiCalled = true;
                        mPageCount += 1;
                        callApi();
                    }
                }
                if (lastVisibleItem == (totalItemCount - 2) && mArtistList.get(totalItemCount - 1) == null)
                    recyclerView.smoothScrollToPosition(totalItemCount);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                clearData();
                tvNoResult.setVisibility(View.GONE);
                String query = etSearch.getText().toString().trim();
                if (Utility.nullCheck(query)){
                    callApi();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                clearData();
                callApi();
            }
        });
    }


    private void clearData() {
        mPageCount =1;
        mArtistList.clear();
        mSearchResultAdapter.notifyDataSetChanged();
        isPaginated=false;
        //tvNoResult.setVisibility(mArtistList.size() == 0 ? View.VISIBLE : View.GONE);
    }

    private void callApi() {
        mSearchPresenter.onCallSearchApi(etSearch.getText().toString().trim(), mPageCount, mSearchId);
    }

    @Override
    public void onRefreshData(List<Artist_> artistList, int searchId) {

        if (checkResultResponse(searchId)) {
            mArtistList.addAll(artistList);
            mSearchResultAdapter.notifyDataSetChanged();
            onNoResultFound();
        }

    }

    @Override
    public void onShowError(String errorMessage, int searchId) {
        if (checkResultResponse(searchId)){
            onNoResultFound();
        }
    }

    private boolean checkResultResponse(int searchId){
        String query = etSearch.getText().toString().trim();
        if (!Utility.nullCheck(query))
            return false;

        if (!isPaginated) {
            clearData();
        }
        if (mSearchId != searchId)
            return false;
        return true;
    }
    public void onNoResultFound() {
        tvNoResult.setVisibility(mArtistList.size() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
        pb.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
        pb.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSearchPresenter.onDestroy();
    }
}

