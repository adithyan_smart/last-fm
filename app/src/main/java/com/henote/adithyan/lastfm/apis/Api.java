package com.henote.adithyan.lastfm.apis;

import com.henote.adithyan.lastfm.pojos.Artist;
import com.henote.adithyan.lastfm.pojos.Results;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

import static com.henote.adithyan.lastfm.pojos.Constants.API_KEY;
import static com.henote.adithyan.lastfm.pojos.Constants.ARTIST;
import static com.henote.adithyan.lastfm.pojos.Constants.FORMAT;
import static com.henote.adithyan.lastfm.pojos.Constants.METHOD;
import static com.henote.adithyan.lastfm.pojos.Constants.PAGE;

/**
 * MOHAN on 17-09-2017.
 */

public interface Api {

    @GET
    Call<Artist> getResult(@Url String url,
                           @Query(METHOD) String method,
                           @Query(ARTIST) String artist,
                           @Query(PAGE) int page,
                           @Query(API_KEY) String apiKey,
                           @Query(FORMAT) String format);
}
