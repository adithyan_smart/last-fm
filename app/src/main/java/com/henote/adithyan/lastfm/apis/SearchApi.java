package com.henote.adithyan.lastfm.apis;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.henote.adithyan.lastfm.R;
import com.henote.adithyan.lastfm.pojos.Artist;
import com.henote.adithyan.lastfm.pojos.Artist_;
import com.henote.adithyan.lastfm.pojos.Results;
import com.henote.adithyan.lastfm.utils.Utility;

import java.util.List;

import butterknife.BindString;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.henote.adithyan.lastfm.pojos.Constants.API_KEY_NAME;
import static com.henote.adithyan.lastfm.pojos.Constants.BASE_URL;
import static com.henote.adithyan.lastfm.pojos.Constants.JSON_FORMAT;
import static com.henote.adithyan.lastfm.pojos.Constants.METHOD_NAME;
import static com.henote.adithyan.lastfm.pojos.Constants.SERARCH_URL;

/**
 * MOHAN on 17-09-2017.
 */

public class SearchApi {

    private Context mContext;
    private SearchResultListener mSearchResultListener;

    @BindString(R.string.no_internet)
    String mNoInternet;

    @BindString(R.string.no_result_found)
    String mNoResultFound;
    public SearchApi(Context context, SearchResultListener searchResultListener){
        mContext = context;
        mSearchResultListener = searchResultListener;
        ButterKnife.bind((AppCompatActivity)mContext);
    }
    public interface SearchResultListener{
        void onSearchResultSuccess(List<Artist_> artistList,int searchId);
        void onSearchResultFailure(String errorMessage,int searchId);
    }

    public void search(final String artist, final int searchId, int page){

        if (mContext == null) return;
        if (!Utility.isOnline(mContext)){
            mSearchResultListener.onSearchResultFailure(mNoInternet, searchId);
            return;
        }

        final Api api= ApiClient.getClient().create(Api.class);
        Call<Artist> getResult = api.getResult(SERARCH_URL, METHOD_NAME,artist,
                page,API_KEY_NAME ,JSON_FORMAT);
        getResult.enqueue(new Callback<Artist>() {
            @Override
            public void onResponse(Call<Artist> call, Response<Artist> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        Results results = response.body().getResults();
                        if (results != null && results.getArtistmatches()!= null){
                            Log.e(getClass().getSimpleName()," result = "+results);
                            List<Artist_> artistList = results.getArtistmatches().getArtist();
                            if (artistList != null && artistList.size()!=0)
                                mSearchResultListener.onSearchResultSuccess(artistList, searchId);
                            else
                                mSearchResultListener.onSearchResultFailure(mNoResultFound, searchId);
                        }
                    }
                }
                else
                    mSearchResultListener.onSearchResultFailure(mNoResultFound, searchId);
            }

            @Override
            public void onFailure(Call<Artist> call, Throwable t) {
                mSearchResultListener.onSearchResultFailure(t.getMessage(), searchId);
            }
        });

    }
}
