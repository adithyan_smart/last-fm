
package com.henote.adithyan.lastfm.mvp;

import com.henote.adithyan.lastfm.pojos.Artist_;

import java.util.List;



public interface SearchListMVP {

    interface SearchView{
        void onRefreshData(List<Artist_> artistList, int searchId);
        void onShowError(String errorMessage, int searchId);
        void showProgress();
        void hideProgress();
    }

    interface SearchPresenter{
        void onCallSearchApi(String query, int page, int searchId);
        void onDestroy();
    }

    interface SearchModel{
        void onCallSearchApi(String query, int page, int searchId);
        interface onSearchResponseListener{
            void onSuccess(List<Artist_> artistList,int searchId);
            void onFailure(String errorMessage,int searchId);
        }
    }
}
