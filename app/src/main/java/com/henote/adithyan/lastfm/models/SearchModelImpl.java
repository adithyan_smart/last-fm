package com.henote.adithyan.lastfm.models;

import android.content.Context;
import android.util.Log;

import com.henote.adithyan.lastfm.apis.SearchApi;
import com.henote.adithyan.lastfm.mvp.SearchListMVP;
import com.henote.adithyan.lastfm.pojos.Artist_;

import java.lang.ref.WeakReference;
import java.util.List;

public class SearchModelImpl implements SearchListMVP.SearchModel, SearchApi.SearchResultListener {
    private Context mContext;
    private SearchListMVP.SearchModel.onSearchResponseListener mOnSearchResponseListener;
    private SearchApi mSearchApi;

    public SearchModelImpl(Context context, SearchListMVP.SearchModel.onSearchResponseListener onSearchResponseListener){
        mContext = new WeakReference<>(context).get();
        mOnSearchResponseListener = onSearchResponseListener;
        mSearchApi = new SearchApi(mContext,this);
    }

    @Override
    public void onCallSearchApi(String query, int page, int searchId) {
        Log.e(getClass().getSimpleName()," query = "+query);
        mSearchApi.search(query,searchId,page);
    }

    @Override
    public void onSearchResultSuccess(List<Artist_> artistList, int searchId) {
        mOnSearchResponseListener.onSuccess(artistList, searchId);
    }

    @Override
    public void onSearchResultFailure(String errorMessage, int searchId) {
        mOnSearchResponseListener.onFailure(errorMessage, searchId);
    }
}
